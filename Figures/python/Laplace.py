import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib

matplotlib.rc('text', usetex = True)
matplotlib.rc('font', **{'family' : "sans-serif"})
params= {'text.latex.preamble' : [r'\usepackage{amsmath}']}
plt.rcParams.update(params)

Nth = 25
Nr = 5
# r = 1 + a*cos(2 th) + b*cos(3 th)
a = 0.25
b = 0.15
th = np.linspace(0, 2*np.pi, Nth)
r = 1 + a*np.cos(2*th) + b*np.cos(3*th)

R = np.array([ np.linspace(0,r1,Nr) for r1 in r])
Th = np.array([ np.ones((Nr,))*t1 for t1 in th])

X2 = R*np.cos(Th)
Y2 = R*np.sin(Th)
Z2 = X2**2 + Y2**2 + 0.25*X2*Y2

xp = r*np.cos(th)
yp = r*np.sin(th)
zp = xp**2 + yp**2 + 0.25*xp*yp

npx = xp
npy = yp
npz = 2*zp

Nx = 13
Ny = 11
x = np.linspace(-1,1,Nx)
y = np.linspace(-1,1,Ny)
X,Y = np.meshgrid(x,y)
Z = X**2 + Y**2 + 0.25*X*Y

# @y=-1, the derivative we want is the y derivative
z1 = x**2 + 1 -0.25*x
nx1 = 0*x
ny1 = 1+0*x
nz1 = 2*(-1) + 0.25*(x)

# @y=1, again y-derivative
z2 = x**2 + 1 + 0.25*x
nx2 = 0*x
ny2 = 1+0*x
nz2 = 2*(1) + 0.25*(x)

# @x=-1, x-derivative
z3 = 1 + y**2 - 0.25*y
nx3 = 1  +0*y
ny3 = 0*y
nz3 = 2*(-1) + 0.25*(-y)

# @x=1, x-derivative
z4 = 1 + y**2 + 0.25*y
nx4 = 1  +0*y
ny4 = 0*y
nz4 = 2*(1) + 0.25*(y)


fig = plt.figure(1, figsize=[8,3])
fig.clf()
ax1 = fig.add_subplot(121, projection="3d")
ax2 = fig.add_subplot(122, projection="3d")
ax1.plot_wireframe(X,Y,Z,color='r', alpha=0.5)

# @ y=-1
ax1.quiver(x, -1+0*x, z1, -nx1, -ny1, -nz1, length=0.5)
# @ y=1
ax1.quiver(x,  1+0*x, z2, nx2, ny2, nz2, length=0.5)
# @ x=-1
ax1.quiver(-1+0*y,  y, z3, -nx3, -ny3, -nz3, length=0.5)
# @ x=1
ax1.quiver(1+0*y,  y, z4, nx4, ny4, nz4, length=0.5)

ax1.quiver(0, 0, 0, 0, 0, 1, length=5, linewidths=4, color='g')
ax1.text(0,0,5, '$\\boldsymbol{\hat{N}}$', color='g', size=20)
ax1.text(1,1,4.0, '$\\boldsymbol{\hat{n}}$', color='b', size=16)

ax2.plot_wireframe(X2,Y2,Z2, color='r', alpha=0.5)
ax2.quiver(xp, yp, zp, npx, npy, npz, length=0.5)
ax2.quiver(0, 0, 0, 0, 0, 1, length=5, linewidths=4, color='g')
# ax2.text(0,0,5, '$\\boldsymbol{\hat{N}}$', color='g', size=20)

ax1.set_xticks([])
ax1.set_yticks([])
ax1.set_zticks([])
ax2.set_xticks([])
ax2.set_yticks([])
ax2.set_zticks([])

ax1.text(1.5,-0.5,0,'$\Delta y$')
ax1.text(0, -1.8, 0,'$\Delta x$')
ax1.text(1.2,1.2,6, '$z$')

ax2.text(1.8, 0.,0,'$\Delta y$')
ax2.text(0, -1.5, 0,'$\Delta x$')
ax2.text(1.2,1.2,6, '$z$')

ax1.text(-1.5,0,6,'(a)')
ax2.text(-2,0,6,'(b)')

ax1.set_zlim([0,6])
ax2.set_zlim([0,6])

fig.tight_layout()
fig.savefig('../Laplace.pdf')
fig.savefig('../Laplace.svg')
plt.show(block=False)

