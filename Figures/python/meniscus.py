import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rc('text', usetex = True)
matplotlib.rc('font', **{'family' : "sans-serif"})
params= {'text.latex.preamble' : [r'\usepackage{amsmath}']}
plt.rcParams.update(params)

h = np.linspace(1e-3,2**0.5, 401)
tmp = (1-h**2/4)**0.5
x = -(2*tmp + 0.5*np.log((1-tmp)/(1+tmp)))

fig = plt.figure(1, figsize=(6,2))
fig.clf()

ax = fig.add_subplot(111)
ax.plot(x-x[-1],h, 'k-', linewidth=2)
ax.fill_between(x-x[-1],h, y2=-0.5, alpha=0.3)
ax.plot(x-x[-1], 0*h+2**0.5, 'k--')
ax.plot(x-x[-1], 0*h, 'k:')
ax.plot([-0.03, -0.03], [-0.5, 1.5], 'k-', linewidth=4)

ax.set_xlim(-0.5, 5)
ax.set_ylim(-0.5,1.5)
ax.set_yticks([0, 0.5, 1.0, 2**0.5])
ax.set_yticklabels(['0', '0.5', '1.0', '$\sqrt{2}$'])

ax.set_xlabel('$x/l_c$')
ax.set_ylabel('$h/l_c$')

ax.arrow(4.5, 0, 0, 2**0.5, width=0.03, shape='full', length_includes_head=True)
ax.text(4.1,0.5, '$h_{max}$')
ax.text(-0.25, 0.5, 'wall', rotation=90)

fig.tight_layout()
plt.show(block=False)

fig.savefig('../meniscus.pdf')
fig.savefig('../meniscus.svg')
