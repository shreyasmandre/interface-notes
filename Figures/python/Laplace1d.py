import matplotlib.pyplot as plt
import numpy as np

s = 0.3
th = np.arcsin(s)
c = np.cos(th)
R = 1/s

Nx = 21
x = np.linspace(-1,1,Nx)
y = R-np.sqrt(R**2-x**2)

fig = plt.figure(1, figsize = (4,1))
fig.clf()
ax = fig.add_subplot(111)

ax.set_xlim([-2,2])
ax.set_ylim([-0.02,1])

ax.plot(x,y,'r-', alpha=0.5, linewidth=4)
ax.arrow(1, (s/2), 0.5, 0.5*s, color='b', shape='full', width=0.02, head_width=0.1)
ax.arrow(-1, (s/2), -0.5, 0.5*s, color='b', shape='full', width=0.02, head_width=0.1)
ax.arrow(0, 0, 0, 0.5, color='g', shape='full', width=0.03, head_width=0.1,)
plt.show(block=False)

