import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as si
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib.gridspec as mg
# from ipywidgets import interact, interactive, fixed, interact_manual
import matplotlib.pylab as pl
from matplotlib import scale
plt.rcParams.update({
  "text.usetex": True,
})

# function that describes the solid surface
h = lambda r,r0,a,delta: a * np.exp(-(np.abs(r)-r0)**2/delta**2)
# function that describes the slope of the solid surface along r
hp = lambda r,r0,a,delta: -2*(np.abs(r)-r0)/delta**2 * h(np.abs(r),r0,a,delta)
hpp = lambda r, r0,a,delta: (-2/delta**2 + 4*(np.abs(r)-r0)**2/delta**4) * h(np.abs(r),r0,a,delta)

def plot_solid_surface(ax, r0,a,delta):
  r = np.linspace(0,1,401)
  ax.plot(r, h(r,r0,a,delta), 'k-')
  ax.plot(-r,h(r,r0,a,delta), 'k-')

def plot_liquid_drop(ax, R, theta, h0=None, color='b'):
  # Here R is the radius of the footprint of the drop
  # and theta is the angle of the spherical cap the drop makes.
  # The optional argument h is to displace the plot vertically by an amount h
  # r = np.linspace(0,R,101)
  R0 = R/np.sin(theta)
  t = np.linspace(-theta, theta, 101)
  r = R0*np.sin(t)
  y = R0*np.cos(t) - R0*np.cos(theta)
  if h0==None:
    h0 = h(R,r0,a,delta)
  y = y*np.sign(theta) + h0
  ax.plot(r,y,'-', color=color)
  ax.fill_between(r,y,y2=h(r,r0,a,delta), color='b', alpha=0.3)
  return r,y

def drop_geometry(R, theta_eq):
  alpha = np.arctan(hp(R,r0,a,delta))
  theta = theta_eq - alpha
  R0 = R/np.sin(theta)
  Vcap = (np.pi/3)*R0**3*(1-np.cos(theta))**2*(2+np.cos(theta))
  Vsurf = np.pi*R**2*h(R,r0,a,delta) - si.quad(lambda x: 4*np.pi*x*h(x,r0,a,delta), 0, R)[0]
  V = Vcap  + Vsurf
  return alpha, theta, R0, V

r0 = 0.5
a = 0.04
delta = 0.03

def hysteresis_curve(ax1, R=None, theta_eq=np.pi/3, plus=False, minus=False):
  Rs = np.linspace(0,1.0,201)
  Vs = 0*Rs
  Vmax = 0*Rs
  Vmin = 0*Rs
  for ii,R1 in enumerate(Rs):
      alpha, theta, R0, Vs[ii] = drop_geometry(R1, theta_eq)
      if ii>0:
          Vmax[ii-1] = np.max(Vs[:ii])
  Vmax[-1] = Vs[-1]

  for ii,R1 in enumerate(Rs):
    Vmin[ii] = np.min(Vs[ii:])

  ax1.plot(Rs, Vs, linewidth=4)
  if plus:
      ax1.plot(Rs, Vmax, 'k--', linewidth=2)
  if minus:
      ax1.plot(Rs, Vmin, 'k--', linewidth=2)
  if R==None:
      b=0
  else:
      alpha, theta, R0, V = drop_geometry(R, theta_eq)
      ax1.plot(R,V,'ro-')
      print('alpha = {0} deg, theta = {1} deg, Radius = {2}, Volume = {3}'.format \
        (alpha*180/np.pi, theta*180/np.pi, R0, V))

  # ax1.arrow(0.45, 0.25,   0.02, 0.2,  color='g', width=0.008, shape="full")
  # ax1.arrow(0.4,  0.15,   0.05, 0.0,  color='g', width=0.008, shape="full")
  # ax1.arrow(0.6,  0.6,    0.15, 0.0,  color='g', width=0.008, shape="full")
  # ax1.arrow(0.85, 0.45,  -0.13, -0.2, color='c', width=0.008, shape="full")
  # ax1.arrow(0.6,  0.1,   -0.05, 0,    color='c', width=0.008, shape="full")
  # ax1.arrow(0.45,-0.025, -0.1, 0,     color='c', width=0.008, shape="full")


def plot_RVPoints(ax1, R, color='r'):
      alpha, theta, R0, V = drop_geometry(R, theta_eq)
      ax1.plot(R,V, 'o', color=color)

def make_plot(ax2, axins=None, R=0.5, theta_eq = np.pi/3, hold=False, color='b'):
  alpha, theta, R0, V = drop_geometry(R, theta_eq)
  plot_solid_surface(ax2, r0, a, delta)
  r,y = plot_liquid_drop(ax2, R, theta, color=color)
  ax2.set_ylim(0,1.5)
  ax2.set_xlim(-1.5,1)
  ax2.set_aspect('equal',adjustable='box')


  if axins==None:
    _=0
  else:
    # ax2.plot([-R-delta, -R-delta,-R+delta,-R+delta], \
    #       [0, 2*delta, 2*delta, 0], 'r-')
    plot_solid_surface(axins, r0, a, delta)
    r,y = plot_liquid_drop(axins, R, theta, color=color)

theta_eq = np.pi/3
R = 0.52
# interact(make_plot, R=(0,1.0,0.01), theta_eq=fixed(np.pi/3), hold=fixed(False))

fig = plt.figure(1, figsize=(8,5))
fig.clf()

gs = mg.GridSpec(2,2, figure=fig)

ax1 = fig.add_subplot(gs[0,0])
ax2 = fig.add_subplot(gs[1,0])
ax3 = fig.add_subplot(gs[0,1])
ax4 = fig.add_subplot(gs[1,1])

ax3ins = inset_axes(ax3, width=1.4, height="50%", loc=2, borderpad=0.3)
ax3ins.set_xticklabels([])
ax3ins.set_yticklabels([])

colors = pl.cm.jet(np.linspace(0,1,9))

hysteresis_curve(ax1, plus=True)

make_plot(ax3, R=0.25, hold=True, color=colors[0] )
make_plot(ax3, R=0.35, hold=True,  color=colors[1] )
make_plot(ax3, R=0.435, hold=True, color=colors[2], axins=ax3ins)
make_plot(ax3, R=0.493, hold=True, color=colors[3], axins=ax3ins)
make_plot(ax3, R=0.5, hold=True,   color=colors[4], axins=ax3ins)
make_plot(ax3, R=0.51, hold=True,  color=colors[5], axins=ax3ins)
make_plot(ax3, R=0.52, hold=True,  color=colors[6], axins=ax3ins)
make_plot(ax3, R=0.808, hold=True, color=colors[7] )
make_plot(ax3, R=0.9, hold=True,   color=colors[8] )
#   ax1.plot(0.435, 0.077, 'kx')
#   ax1.plot(0.493, 0.077, 'kx')
#   ax1.plot(0.52, 0.52, 'kx')
#   ax1.plot(0.808, 0.52, 'kx')

plot_RVPoints(ax1, 0.25,  color=colors[0])
plot_RVPoints(ax1, 0.35,  color=colors[1])
plot_RVPoints(ax1, 0.435, color=colors[2])
plot_RVPoints(ax1, 0.493, color=colors[3])
plot_RVPoints(ax1, 0.5,   color=colors[4])
plot_RVPoints(ax1, 0.51,  color=colors[5])
plot_RVPoints(ax1, 0.52,  color=colors[6])
plot_RVPoints(ax1, 0.808, color=colors[7])
plot_RVPoints(ax1, 0.9,   color=colors[8])


ax1.text(0.425, 0.1, 'A')
ax1.text(0.513, 0.055, 'B')
ax1.text(0.52,  0.55, 'C')
ax1.text(0.78, 0.55, 'D')

ax3ins.set_ylim(-0.1*delta,3*delta)
ax3ins.set_xlim(-R-2*delta,-R+4*delta)
ax3ins.set_aspect('equal',adjustable='box')

hysteresis_curve(ax2, minus=True)

ax4ins = inset_axes(ax4, width=1.4, height="50%", loc=2, borderpad=0.3)
ax4ins.set_xticklabels([])
ax4ins.set_yticklabels([])

make_plot(ax4, R=0.9, hold=True, color=colors[0])
make_plot(ax4, R=0.8, hold=True, color=colors[1])
make_plot(ax4, R=0.7, hold=True, color=colors[2])
make_plot(ax4, R=0.6, hold=True, color=colors[3])
make_plot(ax4, R=0.57, hold=True, color=colors[4], axins=ax4ins)
make_plot(ax4, R=0.502, hold=True, color=colors[5],axins=ax4ins)
make_plot(ax4, R=0.473, hold=True, color=colors[6],axins=ax4ins)
make_plot(ax4, R=0.314, hold=True, color=colors[7],axins=ax4ins)
make_plot(ax4, R=0.2, hold=True, color=colors[8])

plot_RVPoints(ax2, 0.9,   color=colors[0])
plot_RVPoints(ax2, 0.8,   color=colors[1])
plot_RVPoints(ax2, 0.7,   color=colors[2])
plot_RVPoints(ax2, 0.6,   color=colors[3])
plot_RVPoints(ax2, 0.57,  color=colors[4])
plot_RVPoints(ax2, 0.502, color=colors[5])
plot_RVPoints(ax2, 0.473, color=colors[6])
plot_RVPoints(ax2, 0.314, color=colors[7])
plot_RVPoints(ax2, 0.2,   color=colors[8])

ax2.text(0.57, 0.11, "D'")
ax2.text(0.44, 0.175, "C'")
ax2.text(0.50, 0.01, "B'")
ax2.text(0.314, 0.08, "A'")

ax4ins.set_ylim(-0.1*delta,3*delta)
ax4ins.set_xlim(-R-2*delta,-R+4*delta)
ax4ins.set_aspect('equal',adjustable='box')
ax1.set_xlim(0, 1.0)
ax2.set_xlim(0, 1.0)

ax1.set_xticklabels([])
ax3.set_xticklabels([])

ax2.set_xlabel('$R$')
ax1.set_ylabel('$V$')
ax2.set_ylabel('$V$')

ax4.set_xlabel('$x$')
ax3.set_ylabel('$y$')
ax4.set_ylabel('$y$')


ax1.text(0.05,0.95, '(a)')
ax2.text(0.05,0.95, '(c)')

ax3.text(0.8, 1.3, '(b)')
ax4.text(0.8, 1.3, '(d)')


fig.savefig('../Contact_Line_Hysteresis.pdf')
fig.savefig('../Contact_Line_Hysteresis.svg')

fig2 = plt.figure(2, figsize=(5,3))
fig2.clf()
ax = fig2.add_subplot(111)
axins = inset_axes(ax, width=1.4, height="50%", loc=2, borderpad=0.3)
axins.set_xticklabels([])
axins.set_yticklabels([])

make_plot(ax, R=0.45, hold=True,  color=colors[0], axins=axins)
make_plot(ax, R=0.52, hold=True,  color=colors[1], axins=axins)
axins.set_ylim(-0.1*delta,3*delta)
axins.set_xlim(-R-2*delta,-R+4*delta)
axins.set_aspect('equal',adjustable='box')
ax.set_aspect('equal',adjustable='box')

ax.set_xlabel('$x$')
ax.set_ylabel('$y$')
fig2.tight_layout()
fig2.savefig('../SampleDropOnSurface.pdf')
fig2.savefig('../SampleDropOnSurface.svg')
plt.show(block=False)
