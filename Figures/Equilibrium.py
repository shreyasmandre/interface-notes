import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams

params = {'backend': 'Agg',
          'axes.labelsize': 10,
          'axes.titlesize': 10,
          'font.size': 8,
          'xtick.labelsize': 8,
          'ytick.labelsize': 8,
          # 'figure.figsize': fig_size,
          'savefig.dpi': 600,
          'font.family': 'serif',
          # 'font.sans-serif': 'CMU Sans Serif',
          'font.sans-serif': 'Computer Modern Sans serif',
          'axes.linewidth': 0.5,
          'xtick.major.size': 2,
          'ytick.major.size': 2,
          'text.usetex': True,
          'text.latex.preamble': r'\usepackage{amsmath}'}
rcParams.update(params)


d1 = np.loadtxt('Tajima1970-Gamma.png.dat')
d2 = np.loadtxt('Tajima1970-gamma.png.dat')

C2w = d2[:17,0]
gamma2w = d2[:17,1]

C1 = d1[:-1,0]
Gamma = d1[:-1,1]

fig = plt.figure(1, figsize=(3,3))
fig.clf()
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)

ax1.plot(1e3*d1[:,0], d1[:,1]*1e10, 'o-', color='dimgrey', ms=4)
ax2.plot(1e3*d2[:18,0], d2[:18,1], 'o-', color='dimgrey', ms=4)

ax1.set_xlim([0, 12])
ax1.set_ylim([0, 4])
ax2.set_xlim([0, 12])
ax2.set_ylim([30, 75])
ax1.set_xticks([0,2,4,6,8,10,12])
ax2.set_xticks([0,2,4,6,8,10,12])
ax1.tick_params(axis='both', direction='in')
ax2.tick_params(axis='both', direction='in')
ax1.set_xticklabels([])
ax2.set_xlabel('$c$ $\\times 10^{-3}$ M/1000 g solution')
ax2.set_ylabel('$\sigma$ mN/m')
ax1.set_ylabel('$\Gamma$ $\\times 10^{-10}$ M/cm$^2$')

fig.tight_layout(pad=0.1,h_pad=0.01)
fig.savefig('Equilibrium.pdf')
fig.savefig('Equilibrium.svg')

