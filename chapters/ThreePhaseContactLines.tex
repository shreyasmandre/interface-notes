\chapter{Three-phase contact lines}
Now we consider the mechanics at the intersection of the mutual interfaces between three adjoining immiscible phases, the so-called {\it three-phase contact line}.
One of the three phases must be a liquid, so that surface tension between this liquid phase and the other two phases are under tension. 
There are two cases we consider in this chapter: (i) where the three phases are two immiscible liquids and a third fluid, and (ii) where the three phases are liquid, another fluid and a solid.
(Here fluid implies either a liquid or a gas.)

\section{Liquid-liquid-fluid contact line}

\begin{figure}[b]
 \centering
 \includegraphics[keepaspectratio=true]{LiquidLiquidFluidContactLine.pdf}
 % LiquidLiquidFluidContactLine.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{Schematic of a three-phase contact line between two liquids and a third fluid. The three phases are labelled 1, 2 and 3, and occupy a sector of angle $\theta_1$, $\theta_2$ and $\theta_3$, respectively. The surface tension of the interface between phase $i$ and $j$ is $\sigma_{ij}$.}
 \label{fig:LiquidLiquidFluidContactLine}
\end{figure}

At the three-phase contact line between three fluids (naturally, at least two of these must be immiscible liquids), the action of surface tension must form a Neumann triangle.
In other words, the sector angles for the three phases (see Figure~\ref{fig:LiquidLiquidFluidContactLine}a) must adjust themselves in such a way that the force of surface tension from the three interfaces is balanced.
Mathematically, projecting the force along and perpendicular to the 1-3 interface, yields
\begin{subequations}
\label{eqn:neumann}
\begin{align}
 \sigma_{13} + \sigma_{12} \cos\theta_1 + \sigma_{23} \cos\theta_3 = 0, \\
 \sigma_{12} \sin\theta_1 - \sigma_{23} \sin\theta_3 = 0.
\end{align}
\end{subequations}
Either equation \eqref{eqn:neumann} or the geometric interpretation of the Neumann triangle uniquely determine the angles $\theta_1$, $\theta_2$ and $\theta_3$, provided the following inequalities are satisfied:
\begin{subequations}
\label{eqn:llfinequalities}
\begin{align}
 \sigma_{13} &\leq \sigma_{12} + \sigma_{23}, \\
 \sigma_{12} &\leq \sigma_{13} + \sigma_{23}, \text{ and }\\
 \sigma_{23} &\leq \sigma_{12} + \sigma_{13}.
\end{align}
\end{subequations}


\subsection{Application: Two drops in contact}
The Young-Laplace equation and the Neumann triangle construction is used to determine the geometry of two drops (or bubbles) in contact with each other.
The geometry is schematically shown in Figure~\ref{fig:LiquidLiquidFluidContactLine}(b).
The volumes of the two liquid drops ($V_1$ and $V_2$) are known, as are the pairwise surface tensions of the interfaces ($\sigma_{12}$, $\sigma_{13}$, and $\sigma_{13}$).
Each interface takes the shape of a axisymmetric shperical cap. 
The unknowns of the geometry are the angles ($\theta_1$, $\theta_2$, $\theta_{12}$) and the radii ($R_{12}$, $R_{23}$, $R_{13}$) of the spherical caps, which constitute six unknowns. 
To determine these six unknowns, we use the following six conditions:
\begin{enumerate}
 \item The volumes of the two drops depending on the geometry furnish two equations, which are of the form
 \begin{subequations}
 \begin{align}
   V_1 = V(R_{13}, \theta_1) - V(R_{12}, \theta_{12}), \\
   V_2 = V(R_{23}, \theta_2) + V(R_{12}, \theta_{12}),
 \end{align}
 \end{subequations}
 where $V(R, \theta) = 2\pi R^3 (1-\cos\theta)/3$ is a function that represents the volume of spherical cap of radius $R$ and angle $\theta$.
 \item The pressures difference across the three interfaces is given by Laplace pressure, which furnishes one more equations as follows
 \begin{subequations}
  \begin{align}
   p_1 - p_3 = \dfrac{2\sigma_{13}}{R_{13}}, \\
   p_2 - p_3 = \dfrac{2\sigma_{23}}{R_{23}}, \\
   p_2 - p_1 = \dfrac{2\sigma_{12}}{R_{12}}.
  \end{align}
 \end{subequations}
 Here the pressure differences constitute two additional unknowns, which could be eliminated as
 \begin{align}
  \dfrac{2\sigma_{13}}{R_{13}} + \dfrac{2\sigma_{12}}{R_{12}} - \dfrac{2\sigma_{23}}{R_{23}} = 0.
  \label{eqn:twodroplaplace}
 \end{align}
 \item The Neumann triangle condition furnishes two conditions for force balance along two orthogonal directions as
 \begin{subequations}
 \label{eqn:twodropneumann}
  \begin{align}
   \sigma_{13} \cos\theta_1 + \sigma_{23} \cos\theta_2 + \sigma_{12} \cos\theta_{12} & = 0, \label{eqn:twodropneumannx} \\ 
   \sigma_{13} \sin\theta_1 - \sigma_{23} \sin\theta_2 + \sigma_{12} \sin\theta_{12} & = 0. \label{eqn:twodropneumanny}
  \end{align}
 \end{subequations}
 \item Geometric compatibility that radius of the contact line is identical between the three interfaces, which reads
 \begin{align}
  R_{13} \sin\theta_1  = R_{12} \sin \theta_{12} = R_{23} \sin\theta_2. \label{eqn:twodropgeometric}
 \end{align}
 These appear to be two more equations, but indeed \eqref{eqn:twodroplaplace}, \eqref{eqn:twodropneumanny} and \eqref{eqn:twodropgeometric} are dependent on each other, with only three independent equations. This is a remarkable observation because it implies that the consideration of Laplace pressure jumps \eqref{eqn:twodroplaplace} arises out of a combination of vertical force balance \eqref{eqn:twodropneumanny} and geometric compatibility \eqref{eqn:twodropgeometric}. This dependence can be understood as combination of a local force balance (leading to Laplace pressure jumps) forming the global force balance on the contact line (i.e. Neumann triangle). 
\end{enumerate}
These six independent equations in six unknowns can be solved to determine the resultant geometry of the two-drop complex.

\section{Solid-liquid-fluid contact line}
Considerations change when one of the phases is a rigid solid so its interface with the other phases does not deform. 
A schematic of this situation is shown in Figure~\ref{fig:SolidLiquidFluidContactLine}, where without loss of generality, we denote the third fluid to be air.
The surface tension on the liquid-air, solid-liquid and solid-air interfaces is denoted $\sigma_{la}$, $\sigma_{sl}$ and $\sigma_{sa}$, respectively.
Since only the liquid-air interface is deformable in this situation, let us say that it makes an angle $\theta$ with the solid surface.

\subsection{Equilibrium based on energetic considerations}
An important variable that determines whether and how the liquid spreads on the solid surface is the differential change in surface excess energy caused by the motion of the interface.
If the contact line in Figure~\ref{fig:SolidLiquidFluidContactLine}(a) displaces a distance $d$ away from the liquid phase, then this contributes to an additional solid-liquid excess energy of $\sigma_{sl}d$, and a reduction in the solid-air excess energy of $\sigma_{sa}d$. 
However, such a displacement $d$ would cause the liquid-air interface to expand a distance $d\cos\theta$, costing an additional excess energy of $\sigma_{la}d\cos\theta$.
Since thermodynamic systems tend to reduce the excess energy, the decrease in excess energy $(\sigma_{sl}-\sigma_{sa} +\sigma_{la}\cos\theta)d$ can be considered to be a driving force behind the spreading of the liquid.
This quantity may be written in terms of the wetting parameter $S$ defined as
\begin{align}
 S = \dfrac{\sigma_{sa} - \sigma_{sl}}{\sigma_{la}}.
\end{align}
The decrease in excess energy is $(\cos\theta - S) \sigma_{la} d$.

First, suppose $-1 \leq S \leq 1$. In this case, the contact line has a tendency to advance (i.e. the liquid wets more surface of the solid) if $\cos\theta < S$, and the contact line has the tendency to recede (i.e. the liquid wets less solid surface) if $\cos\theta > S$. 
Equilibrium corresponds to the case between the two tendencies, i.e. $\cos\theta = S$. The angle that satisfies this condition is called the {\it equilibrium contact angle}, which we will denote $\theta_\text{eq}$.
Thus,
\begin{align}
 \cos\theta_\text{eq} = S = \dfrac{\sigma_{sa} - \sigma_{sl}}{\sigma_{la}}.
\end{align}
This relation is called the {\it Young-Dupr\'{e}} relation.

Now, let us consider the case $S>1$. 
(The case $S<-1$ may be treated by exchanging the roles of liquid and air.) 
The condition $S>1$ may be interpreted as $\sigma_{sa} > \sigma_{sl} + \sigma_{la}$. 
If the solid surface is coated by a microscopically thin layer of the liquid, as shown in Figure~\ref{fig:SolidLiquidFluidContactLine}(b), the equivalent excess energy of the surface per unit area is $\sigma_{sa,\text{eq}} = \sigma_{sl} + \sigma_{la}$.
When $S>1$, this state of the liquid coated solid surface is therefore thermodynamically favourable compared to the pristine solid-air interface.
As a consequence, any vapour of the liquid present in the environment spontaneously condenses on the solid surface coating it upto a point where the equivalent excess energy $\sigma_{sl} + \sigma_{la}$ becomes equal to $\sigma_{sa}$. 
In this manner, unless extreme care is exercised to maintain the purity of the interface, surfaces with large values of surface excess energy are spontaneously contaminated by chemical elements present in the environment that reduce the equivalent excess surface energy.
Thus, for materials which posses $S>1$, contaminants spontaneously reduce the excess surface energy to an equivalent value of 1.
For this reason, the case $S>1$ reduces to the situation $S=1$ (and by argument of exchanging the fluid and the liquid $S<-1$ reduces to $S=-1$).

\subsection{Equilibrium based on force balance considerations}\label{sec:youngdupreforce}
We now present the same results based on force-balance considerations.

\begin{figure}[b]
 \centering
 \includegraphics[keepaspectratio=true]{SolidLiquidFluidContactLine}
 % SolidLiquidFluidContactLine.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{Schematics for analysis of the solid-liquid-fluid contact line. (a) Energetics and force balance at a solid-liquid-fluid contact line. (b) A solid surface coated by layer of the lquid.}
 \label{fig:SolidLiquidFluidContactLine}
\end{figure}
The unbalanced surface tension forces (per unit length perpendicular to the plane of the paper) acting at the contact line is
\begin{align}
\sigma_{sl} + \sigma_{la} \cos\theta - \sigma_{sa} = (\cos\theta - S) \sigma_{la} = (\cos\theta - \cos\theta_\text{eq}) \sigma_{la}.
\end{align}
When $\cos\theta > \cos\theta_\text{eq}$, i.e. $\theta < \theta_\text{eq}$, the unbalanced force points into the liquid and the contact line tends to recede and de-wet the solid, whereas when $\cos\theta< \cos\theta_{eq}$, i.e. $\theta>\theta_\text{eq}$, the unbalanced force points away from the liquid and tends to advance the contact line and the liquid to wet the solid.
Equilibrium is attained at $\theta = \theta_\text{eq}$ when the unbalanced force vanishes.

\subsection{Contact line motion}
Contact lines move in the direction of unbalanced tangential force.
The seed with which they move is a topic of current research, but there is no doubt that they move in te direction of unbalanced tangential force, which as argued earlier depends on $(\cos\theta - \cos\theta_\text{eq})$.

The idealized relations derived in \S\ref{sec:youngdupreforce} can only hold for truly flat surfaces. However, real surfaces are rarely flat, which has non-trivial consequences for the motion of the contact line. 
These consequences are expressed in the literature using terms such as {\it pinning, de-pinning, stick-slip motion, and contact line hysteresis}.
We present a rationalization of these phenomena here.
\begin{figure}[h]
 \centering
 \includegraphics[keepaspectratio=true]{CurvedSolidSurface.pdf}
 % CurvedSolidSurface.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{Influence of surface curvature on the tendency of a contact line to move. (a) Solid surface with a convex face towards the liquid. Solid black curved arrow denotes imagined displacement of the contact line from $O$ to $O'$. This displacement reduces the contact angle of the liquid because of the change in orientation of the solid surface and results in a unbalanced force that tends to move the contact line back to $O$. Therefore, the contact line position in this case is stable. (b) Solid surface with concave face towards the liquid. In this case, the contact angle at the displaced contact line at $O'$ in higher than its original position $O$, and the resulting imbalance in the surface tension forces tends to move the contact line further away from $O$. Therefore, the contact line position in this case is unstable and runs away from $O$. }
 \label{fig:CurvedSolidSurface}
\end{figure}

To understand the basic mechanisms, consider the influence of curvature of a solid surface on the tendency of contact line motion.
The two possibilities of convex and concave surfaces is shown schematically in Figure~\ref{fig:CurvedSolidSurface}.
In the first case, Figure~\ref{fig:CurvedSolidSurface}(a) shows the contact line on a curved solid surface with the convex face towards the liquid.
The liquid initially makes an angle equal to the equilibrium contact angle at the contact line.
If we now imagine a small displacement of the contact line as shown in this figure, the contact angle changes in such a way as to push the contact line back to its original location.
This contact line configuration is stable. 
However, if the concave face of the solid is towards the liquid, as shown in Figure~\ref{fig:CurvedSolidSurface}(b), the change in contact angle pushes the contact line further away from its initial location.
This contact line configuration is unstable and runs away from its original location.

\begin{figure}
 \centering
 \includegraphics[width=5in]{SampleDropOnSurface}
 \caption{Schematic of an axisymmetric drop on an  axissymetric surface. The equilibrium contact angle  between the solid and the liquid is 60$^\circ$. The solid surface has a small axisymmetric bump at a radius of 0.5 from the axis of symmetry. The figure shows the drop shape for two radii of the contact line, one right before the uphill part of the bump and another on its downhill part.}
 \label{fig:SampleDropOnSurface}
\end{figure}

Next we use the background developed so far to discuss the phenomena emerging from the interaction of contact lines with surfaces that possess microscopic features.
To illustrate the simplest such phenomena, consider a axisymmetric drop on a surface with a well-defined concentric geometric asperity, such as the one shown in Figure~\ref{fig:SampleDropOnSurface}.
The surface is otherwise smooth and offers a fixed equilibrium contact angle to the liquid in the drop.
Consider the asperity to be miscroscopic, i.e. of a length scale much smaller than the drop radii in question.
Depending on the volume of the deposited liquid, the equilibrium shape of the drop is a spherical cap with a contact-line radius $R$. 

\begin{figure}
 \includegraphics[width=7.5in]{Contact_Line_Hysteresis}
 \caption{Contact line motion, pinning, depinning, stick-slip and hysteresis. (a) Volume of the drop $V$ as a function of the contact line radius $R$ for a drop (blue curve). The dashed curve shows the path taken by a drop of increasing volume. The contact line radius jumps from A to B and then from C to D. Coloured solid circles show specific cases illustrated in panel b. (b) The shape of the drop at the points indicated by solid coloured circles in panel a (corresponding cases coded by identical colour). (c-d) Same as panels a-b but for drops of decreasing volume.}
 \label{fig:Contact_Line_Hysteresis}
\end{figure}

Now imagine increasing the volume of the deposited drop on the surface, which advances the contact line. 
In doing so, the drop volume traverses the path shown in Figure~\ref{fig:Contact_Line_Hysteresis}(a) and the shapes in Figure~\ref{fig:Contact_Line_Hysteresis}(b).
For small volumes, the contact line of the deposited drop has a radius smaller than the asperity.
As the volume increases and the contact line approaches the asperity, the surface is concave to the liquid and, therefore, unstable.
This causes the contact line to slip over a small region, depicted in Figure~\ref{fig:Contact_Line_Hysteresis}(a) from points A to B.
On the convex part of the solid surface, the contact line is stable, and offers a range of microscopic orientations to the liquid over the extent of the asperity. 
This process is depicted in Figure~\ref{fig:Contact_Line_Hysteresis}(a) between points B and C, and the corresponding interface shapes are shown in Figure~\ref{fig:Contact_Line_Hysteresis}(b).
As the contact line traverses the asperity, while the microscopic contact angle the liquid surface makes with the local solid surface remains equal to the equilibrium contact angle, the apparent or the ``macroscopic'' contact angle traverses a range equal to the range of the surface orientation.
In this range, the contact line itself moves only by an amount comparable to the asperity scale.
For microscopic asperities, the contact line appears to be {\it pinned} at the asperity, while the the angle can take a range of values, say $\theta_\text{min} \leq \theta \leq \theta_\text{max}$, depending on the precise details of the shape of the asperity.
If the volume of the drop exceeds beyond that of point C in Figure~\ref{fig:Contact_Line_Hysteresis}(a), the contact line {\it depinns} and slips to the position given by D. 

Now imagine decreasing the volume below that given by point D, which recedes the contact line.
The drop shape then evolves along the geometry described in Figure~\ref{fig:Contact_Line_Hysteresis}(c-d).
On the way towards deflating the drop, the path taken by the drop jumps from D' to C' and B' to A', which is distinct from the path taken while inflating.
The contact line pins and depins, but between different positions compared to an advancing contact line. 
The difference in paths taken by an advancing contact line versus a receding contact line is termed as {\it contact line hysteresis.}

A similar picture is expected at chemical asperities on a flat solid surface.
A chemical asperity is a region of the surface where the equilibrium contact angle varies from the rest of the surface.
Here we state without proof or demonstration that phenomena similar to pinning, depinning, stick-slip behaviour and contact line hysteresis also exists in the presence of chemical asperities.

The roughness of real surfaces is best characterized as random, composed of a distribution of geometric and chemical asperities on the microscale. 
Because of the roughness, the contact line pins can pin at any location and only advance if the contact angle exceeds a value $\theta_\text{adv}$ called the advancing contact angle, which is greater than the equilibrium contact angle on a flat surface.
Similarly, the contact line recedes when the contact angle decreases below $\theta_\text{rec}$ called the receding contact angle, which is smaller than the equilibrium contact angle.


\section{Conclusion}
In this chapter, we have presented conditions of mechanical equilibrium at the three-phase contact line.
The nature of the condition depends on whether all three phases are deformable, or whether one of the three phases is rigid.
When one of the phases is rigid, mechanical equilibrium demands the satisfaction of the Young-Dupr\'{e} relation.
However, microscopic geometric and chemical asperities on the rigid surface then lead to phenomona of contact line pinning, depinning, stick-slip and hysteresis. 
For deformable phases, a Neumann triangle condition on the orientation of the interface applies.
In recent years, experiments have confirmed the validity of the Neumann triangle condition for soft solids, such as hydrogels \cite{Style_2017}. 

Another topic we did not discuss in this chapter is that of contact line behaviour on textured surfaces, i.e. surfaces with structured microscopic roughness.
Texture systematically changes the energetic considerations of the interface with the solid surface.
This leads to the lotus leaf effect, a phenomenon where droplets of water roll off of lotus leafs without wetting the surface.
The explanation invokes a state of the interface called the {\it Cassie-Baxter state}, where the droplet balances gently on the tips of the microstructure without wetting the whole solid surface.
However, the effect could be disrupted by transitioning to the so-called {\it Wenzel state}, where the liquid wets the whole surface. 
We will be seeing much more research on these topic in the coming years.

This completes the preliminaries necessary to mathematically model physical phenomena with liquid interfaces.
\bibliographystyle{plain}
\bibliography{bib/interfaces}
