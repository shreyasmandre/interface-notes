\chapter{Static equilibrium of liquid interfaces}\label{ch:mechanics}

Surface tension contributes to the balance of forces on the fluid at the interface.
We will consider the configuration of the static interfaces on the presence of surface tension.

\section{Planar liquid interfaces}\label{sec:planarinterfaces}
Consider a patch of planar liquid interface, as shown in Figure~\ref{fig:BasicFreeBody}.
The patch could enclose the liquid interface itself or some other material, such as a floating body.
Let the boundary of the patch be denoted by the closed curce $C$. 
According to \eqref{eqn:forceduetosurftens}, the net force on the patch due to surface tension is
\begin{align}
 \bF = \int_C \sigma \nhat~\dd l,
\end{align}
where $\nhat$ is a unit vector normal to $C$ but tangent to the interface and $\dd l$ is the arc-length element.
If the coefficient of surface tension is constant along the curve, then the integral can be demonstrated to vanish as follows:
\begin{align}
 \sigma \int_C \nhat \dd l = -\sigma \bN \times \int_C \dd \br = \sigma \bN \times \boldsymbol{0} = \boldsymbol{0},
\end{align}
where $\bN$ is a unit vector normal to the interface (a constant for planar interfaces), and $\br$ is the differential vector arc-length element.
The integral of $\dd \br$ integrates to zero because it is an exact differential and the integral is carried over a closed curve.

Another way to derive this result is to apply the divergence theorem as
\begin{align}
 \bF = \int_C \sigma \nhat ~\dd l  = \int_A \grad\sigma ~ \dd A, 
\end{align}
where $A$ is the area enclosed by the curve $C$.
Here $\grad\sigma$ vanishes if $\sigma$ is constant.

Based on this, we conclude that an unbalanced force can exist on a planar interface only if the surface tension is not constant.
This unbalanced force is necessarily tangential to the surface.
This force may be considered to be composed on an infinite number of infinitesimal contributions $\dd \bF$ arising from small area elements $\dd A$ making up the interface patch as
\begin{align}
 \dd \bf = \grad\sigma~\dd A.
\end{align}

Although the force of surface tension is proportional to the length of the curve $C$, for planar interfaces the unbalanced force on infinitesimal patches is not proportional to the perimeter of the patch but the area of the patch.
This is essentially due to the direction along which the force of surface tension applies -- always tangent to the interface and normal to the curve being considered.
Thus, we have the manifestation of a quantity that behaves like a stress.
This quantity, $\grad \sigma$ is called the \textit{Marangoni stress}, after the Italian physicist Carlo Marangoni (1840-1925). 
These gradients of surface tension may exist on the interface due to non-uniform distribution of temperatures, solvents or surfactants (such as soap). 

Let us now consider the influence of curvature of the interface.

\begin{figure}
 \centering
 \includegraphics[keepaspectratio=true,width=0.95\textwidth]{Laplace}
 % Laplace.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{Schematic of free body diagram on an infintesimal element of a curved interface. (a) An approximately rectangular element of the interface aligned with the Cartesian axes. Red mesh indicates the interface patch, blue arrows the force of surface tension, and the green arrow indicates the unit normal to interface $\hat\bN$. (b) An infintesimal patch of the interface of arbitrary shape.}
 \label{fig:Laplace}
\end{figure}

\section{Curved liquid interfaces}\label{sec:curvedinterfaces}

For a curved interface, the force balance also has a component normal to the interface.
This can be readily seen by examining the schematics shown in Figure~\ref{fig:Laplace}, where it can be seen that the component normal to the interface does not cancel out.
\subsection{Derivation for a rectangular interface patch}
To derive the magnitude of this net unbalanced force, let us first consider an infinitesimal patch of the interface, approximately rectangular in shape with sides $\Delta x \times \Delta y$ (see Figure~\ref{fig:Laplace}a). 
Here we take two orthogonal directions in the tangent plane of the interface to be the $x$ and the $y$ axes of a Cartesian coordinate system, with its origin at the centre of the rectangular patch.
The interface normal is then along the $z$ axis.
The sum of the forces on the opposite sides, say those aligned along the $y$ direction, can be evaluated as follows.
Using the sketch in Figure~\ref{fig:Laplace1d}, the component of the interfacial force in the direction normal to the interface is 
\begin{align}
 \sigma \Delta y \times 2 \sin \theta \approx \sigma \Delta y \times 2 \theta \approx \sigma \Delta y \times \kappa_{xx} \Delta x,
\end{align}
where $\kappa_{xx}$ is the curvature of the interface patch along the $x$ direction, such that $\theta = \kappa_{xx} \Delta x/2$.
Here, the force along the tangent place to the interface vanishes.
(If surface tension is not constant, then the tangential component evaluates to the Marangoni stress derived in \S\ref{sec:planarinterfaces}.)
Similarly, the unbalanced force on the sides aligned with the $x$ direction is
\begin{align}
 \sigma \Delta x \times \kappa_{yy} \Delta y,
\end{align}
where $\kappa_{yy}$ is the interface curvature along the $y$ direction.
The net force may be written as
\begin{align}
 \int_{C} \sigma \nhat ~\dd l ~ \approx ~ \sigma \Delta x \Delta y \times (\kappa_{xx} + \kappa_{yy}).
\end{align}
Noting that $\kappa_{xx} + \kappa_{yy}$ is twice the mean curvature $\kappa_\text{mean}$, and in the limit of an infinitesimal patch size, we arrive at
\begin{align}
 \int_{C} \sigma \nhat ~\dd l ~ \approx ~ \sigma \Delta x \Delta y \times 2 \kappa_\text{mean} = \sigma \Delta A \times 2 \kappa_\text{mean},
\end{align}
where $\Delta A$ is the area of the patch.
The result is independent of the choice of $x$ and $y$ axes direction, so long as they are along the tangent plane to the interface, because the mean curvature is an invariant of the interface geometry.

\begin{figure}
 \centering
 \includegraphics[keepaspectratio=true]{Laplace1d}
 % Laplace.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{Side perspective of a section of the interface in Figure~\ref{fig:Laplace}(a). The side visible is of length $\Delta x$. Color code same as Figure~\ref{fig:Laplace}.}
 \label{fig:Laplace1d}
\end{figure}

\subsection{General derivation for a curved interface patch}
To see that the result is independent of the shape of the patch, and that the Marangoni component of the force is additive, let us consider the net force on an infinitesimal patch of the interface of arbitrary shape, such as shown in Figure~\ref{fig:Laplace}(b).
The tangent plane is again used to construct a local Cartesian coordinate system with its origin inside the patch, with the $x$ and $y$ coordinates parameterizing the patch.
The curvature at the origin is, say, given by the tensor
\begin{align}
 \bkappa = \begin{bmatrix} \kappa_{xx} & \kappa_{xy} \\ \kappa_{xy} & \kappa_{yy} \end{bmatrix}.
\end{align}
The normal to the interface at any location parameterized by the coordinates $(x,y)$ is given to $O(x,y)$ by 
\begin{align}
 \hat\bN = \zhat + (\kappa_{xx} x + \kappa_{xy} y) \xhat + (\kappa_{xy} x + \kappa_{yy}y) \yhat.
\end{align}
Noting that $\nhat = \that \times \Nhat$ and that $\that = a \xhat + b \yhat + O(x^2, xy, y^2)$,  i.e. it lies in the $xy-$plane, yields
\begin{align}
 \nhat = \nhat' + \zhat \left\{ \nhat' \cdot \left[ (\kappa_{xx} x+\kappa_{xy}y)\xhat + (\kappa_{xy}x+\kappa_{yy}y)\yhat \right] \right\} + O(x^2,xy,y^2),
\end{align}
where $\nhat' = \that \times \zhat = a \yhat - b \xhat$ is the unit vector in the $xy$-plane that is perpendicular to $\that$.
The net force evaluates to
\begin{align}
 \begin{split}
 \int_C \sigma \nhat~\dd l &= \int_C \sigma \nhat'~\dd l +  \zhat \int_C \sigma~\nhat\cdot\left[ (\kappa_{xx} x+\kappa_{xy}y)\xhat + (\kappa_{xy}x+\kappa_{yy}y)\yhat \right] \dd l + O(x^3,x^2y,xy^2,y^3) \\
 &= \int_A \grad\sigma ~\dd A + \zhat \int_A \sigma \left[ \pd{(\kappa_{xx}x+\kappa_{xy}y)}{x} + \pd{(\kappa_{xy}x + \kappa_{yy}y)}{y} \right] \dd A + O(x^3,x^2y,xy^2,y^3)  \\
 &= \int_A \grad\sigma ~\dd A + \zhat \int_A \sigma (\kappa_{xx} + \kappa_{yy}) \dd A + O(x^3,x^2y,xy^2,y^3),
 \end{split}
\end{align}
where the order of the error term arises from the product of $O(x^2,xy,y^2)$ error in the integrand and the $O(x,y)$ extent of the integration domain along the length of the curve.
In the limit of an infinitesimal interface patch, the integrals scale as the area of the patch , i.e. $O(x^2,xy,y^2)$, while the error term is one order smaller, and thus vanishes.
This leads to
\begin{align}
 \int_C \sigma \nhat~\dd l = \underbrace{\int_A \grad\sigma ~\dd A}_{\text{Marangoni stress}} + \zhat \underbrace{\int_A \sigma (\kappa_{xx} + \kappa_{yy}) \dd A}_{\text{Laplace pressure}}.
 \label{eqn:generalcurvedinterfaces}
\end{align}


As in \S\ref{sec:planarinterfaces}, for flat interfaces, \eqref{eqn:generalcurvedinterfaces} reduces to the Marangoni stress and for constant $\sigma$, it derives its contribution only from Laplace pressure.

\begin{figure}
 \centering
 \includegraphics[keepaspectratio=true]{Laplace2d}
 % Laplace.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{Definition of some useful vectors in Figure~\ref{fig:Laplace}(b). Here $\that$ is the unit tangent vector to the boundary curve $C$, $\Nhat$ the unit normal to the interface and $\nhat$ the vector normal to $\that$ but parallel ot the interface.}
 \label{fig:Laplace2d}
\end{figure}

\subsection{Consequences of Laplace pressure -- equilibrium shape of a liquid blob}
The results so far can be used to determine the shape of a liquid blob in equilibrium.
For this determination, we will ignore the influence of gravity.
Equilibrium dictates that the surface tension be uniform, i.e. agencies such as temperature and surfactants that cause surface tension to vary are uniform. 
In the absence of gravity, hydrostatics dictate that the pressure inside $p_\text{in}$ and outside $p_\text{out}$ the drop must also be uniform.
The force balance on a small patch of the interface then only consitutes the forces of pressure and those acting on the interface as
\begin{align}
 \dd \bF = \int_A \left(p_\text{in} - p_\text{out} -2 \sigma \kappa_\text{mean}\right) ~\dd A = 0.
\end{align}
Consequently, 
\begin{align}
 p_\text{in} = p_\text{out} + 2 \sigma \kappa_\text{mean},
\end{align}
or, in other words, the inside of the blob is pressurized relative to the outside by an amount equal to the Laplace pressure.
Since the inside pressure must be uniform, the shape of the blob must be one with constant mean curvature, i.e. a sphere.

Energetic considerations may alternatively be used to arrive at the same conclusion.
The thermodynamic energy of the system is derived from the surface energy of the blob.
Minimizing this energy is then equivalent to determining the shape the minimizes the surface area, while maintaining a fixed volume equal to the volume of the blob.
This shape is a sphere.

