\chapter{Introduction to surface tension}\label{ch:introduction}
The interface between a liquid and its adjoining material (may it be another liquid, a solid, or a gas) is naturally under a mechanical state of tension. 
It is as if the liquid interface wants to shrink in area, but is held stretched by some forces, just as the rim of a trampoline holds the trampoline sheet in a stretched state (see Figure~\ref{fig:Waterstrider} for an illustration). 
This property of a liquid interface is called \textit{surface tension}.
\begin{figure}[h!]
 \centering
 \includegraphics[width=80mm,keepaspectratio=true]{WaterStrider.jpg}
 % WaterStrider.jpg: 2508x1672 px, 72dpi, 88.48x58.98 cm, bb=0 0 2508 1672
 \caption{A Waterstrider floating on the surface of water without sinking. The weight of the organism makes depressions in the air-water interface, indicating the tension present on the interface. (source: \url{pxhere.com/en/photo/889347})}
 \label{fig:Waterstrider}
\end{figure}

Mechanically, the surface tension is represented by an internal force per unit length acting along the interface. By an internal force, we mean a force exerted by one part of the interface on another (see Figure~\ref{fig:Introduction}). It is this internal force that keeps the entire interface in tension. 

\begin{figure}[b]
 \centering
 \includegraphics[keepaspectratio=true]{IntroductionSchematic}
 % IntroductionSchematic.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{Schematic illustrating the concept of surface tension. (a) A container containing a liquid. The panel depicts the liquid-air interface. (b) The interface is under a state of tension depicted schematically by the arrows. The strength of this tension is represented by a force per unit length with magnitude $\sigma$.}
 \label{fig:Introduction}
\end{figure}

Free body diagrams are indispensible tools for analyzing the mechanical consequences of surface tension. For a patch of the interface, whose boundary is given by the curve $C$, the net force exterted by the neighbouring portion of the interface is given by
\begin{align}
 \bF = \int_C \sigma \hat\bn ~ \dd l,
 \label{eqn:forceduetosurftens}
\end{align}
where $\sigma$ is the coefficient of surface tension, $\hat\bn$ is a unit normal that is tangential to the interface but normal to the curve $C$, and $\dd l$ is the arc-length elment.

\begin{figure}
 \centering
 \includegraphics[keepaspectratio=true]{BasicFreeBody}
 % BasicFreeBody.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{Free body diagram depicting the force due to surface tension exerted on a patch of a liquid interface by its neighbouring portion.}
 \label{fig:BasicFreeBody}
\end{figure}

In this chapter, we discuss the molecular origin of surface tension. 

\section{Molecular origin of surface tension}\label{sec:molecularorigin}
The reason underlying the existence of surface tension on liquid interfaces may be traced back to the reason underlying the existence of the liquid state itself. The chain of reasoning justifying this claim is briefly and informally presented below. For more details, refer to j. Israeliachvili, ``Intermolecular and surface forces'', Academic Press, London. 

The liquid state exists due to a weak attractive force between the molecules of the liquid. 
In the absence of such an attractive force, the interaction between the molecules is limited to a short range repulsion, causing collision between the molecules, and leading to a gaseous state.
Indeed, an assumption underlying the kinetic theory of gases is that attractive interaction between molecules be negligible (compared to their average kinetic energy).
In the absence of such an interaction, the molecules move more of less in a straight line between collisions and this fill the whole volume available to them, which is the defining characteristic of a gas.
Violation of this assumption causes a departure from gaseous state and underlies the formation of the liquid state.

A gas condenses when the average kinetic energy of the molecules becomes comparable to the potential energy well formed by a combination of long-range attraction and short-range repulsion between molecules.
As the temperature of the gas is reduced, the kinetic energy of the molecules decreases (the kinetic energy is proportional to the temperature according to statistical mechanics). 
At the melting point, the kinetic energy is comparable to the intermolecular attractive potential energy.

\begin{figure}[h]
 \centering
 \includegraphics[keepaspectratio=true]{Intermolecular}
 % Intermolecular.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{A characteristic interaction potential energy between two molecules of a liquid. }
 \label{fig:Intermolecular}
\end{figure}


The interaction between molecules may be characterized by an intermolecular potential energy.
The depth of the potential well, $\epsilon$, characterizes the scale for the energy needed for molecules to escape from the attractive interaction (see Figure~\ref{fig:Intermolecular}).
This energy is provided to the molecules in the form of random thermal kicks provided by the jiggling of the neighbouring molecules.
The magnitude of these kicks is characterized in terms of the average kinetic energy of the molecules, which is given in terms of te absolute temperature as $k_B T$ ($k_B$ being the Boltzmann constant).
When $\epsilon \approx k_B T$, molecules start to coagulate because the thermal jiggling is not strong enough to break the attractive bond between the molecules. 
This is how a gas condenses, typically into a liquid state for small molecules like water.

Thus, an order of magnitude estimate for the interparticle potential well $\epsilon \approx k_B T_m$, where $T_m$ is the boiling temperature.
For water, $T_m = 373$ K, which implies $\epsilon \approx k_B T_m = (1.3 \times 10^{-23}$ J/K) $(373)$ K $\approx 5 \times 10^{-21}$ J. 
For liquid Helium, $T_m \approx 4$ K, so $\epsilon \approx 6 \times 10^{-23}$ J.

\begin{figure}[b]
 \centering
 \includegraphics[keepaspectratio=true]{NearestNeighbours}
 % NearestNeighbours.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{Nearest neighbour interaction for liquid molecules in the interior of the liquid phase and for molecules near the interface.}
 \label{fig:NearestNeighbours}
\end{figure}

Thus, the energy of each molecule is reduced by an amount that scales as $Z\epsilon$, where $Z$ is the average number of neighbours the molecule experiences, approximately 8 (see Figure~\ref{fig:NearestNeighbours}).
At the interface, the molecules have fewer neighbours, say half that of a molecule in the interior.
These molecules have a higher energy due to the reduced number of neighbours.
The magnitude of this energy per molecule may be estimated as $Z\epsilon/2$.
If $a$ is half the typical intermolecular distance, the excess energy per unit area scales as $Z\epsilon/(2 \pi a^2)$.
Using $a \approx 1$ nm for small molecules like water, the excess surface energy per unit area is 
\begin{align}
\epsilon_A = \dfrac{8}{2} \dfrac{5 \times 10^{-21} \text {J}}{3.14 \times (10^{-9} \text{ m})^2} \approx 6 \times 10^{-3} \text{ J/m}^2.
\end{align}
Similarly, for helium
\begin{align}
\epsilon_A = \dfrac{8}{2} \dfrac{6 \times 10^{-23} \text {J}}{3.14 \times (10^{-9} \text{ m})^2} \approx 7 \times 10^{-5} \text{ J/m}^2.
\end{align}
This excess energy is closely related to the surface tension.

Excess energy means lower density of molecules at the interface.
The molecules at the interface are attracted towards the interior of the liquid. 
Think of either the unbalanced force or the excess energy for being on the interface. 
It is energetically not preferable for the molecules to be at the interface.
As a result of this attraction, the density of molecules on the interface is less than that in the bulk, as shown schematically in Figure~\ref{fig:NearestNeighbours}.

Lower density means larger average distance between molecules on the interface.
The average intermolecular distance lies in the range for long-distance attraction.
This attraction manifests as a tensile force on the interface, or surface tension.
In this manner, an attractive intermolecular force leads both to the existence of the liquid state and the surface tension on the interface.

\section{Surface energy and surface tension}
There is a closer relation between excess surface energy and surface tension.
Consider the following thought experiment.
A trough with a movable wall is filled with a liquid as shown in Figure~\ref{fig:Trough}.
The trough has width $w$ out of the plane of the paper.
The way we defined surface tension, the interface exerts a force $\sigma w$ on the barrier.
Imagine a situation with zero gravity.
\begin{figure}
 \centering
 \includegraphics[keepaspectratio=true]{Trough}
 % Trough.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{Schematic of the thought experiment to illustrate the relation betwee excess surface energy and surface tension. (a) A column of liquid in a trough with a movable barrier. (b) The barrier moved a distance $d$ to extend the interface.}
 \label{fig:Trough}
\end{figure}

Now imagine moving the barrier quasi-statically a distance $d$, as shown in Figure~\ref{fig:Trough}(b). 
The area of the interface increases by an amount $wd$, hence the excess interface energy increases by $\epsilon_A \times wd$.
The work done by the force moving the barrier is $(\sigma w) \times d$.

By the law of conservation of energy the work done to move the wall must all appear as an increase in the excess surface energy, because there is no other mechanism for the energy to be stored.
As a result
\begin{align}
 \epsilon_A \times wd = (\sigma w) \times d \qquad \Longrightarrow \qquad \sigma = \epsilon_A,
\end{align}
i.e. the excess surface energy is numerically equal to the surface tension.

The dimensions of the two quantities are also identical.
The dimensions of $\epsilon_A$ are [E/L$^2$], where [E] is the dimension of energy, while dimensions of $\sigma$ are [F/L], where [F] is the dimension of force. 
Because [E] = [FL] (energy is force times displacement), the dimensions of $\sigma$ and $\epsilon_A$ are identical.

We estimated the excess surface energy of water and liquid helium to be $6\times 10^{-3}$ J/m$^2$ and $7 \times 10^{-5}$ J/m$^2$, respectively. 
The corresponding values of surface tension are $70 \times 10^{-3}$ N/m and $3 \times 10^{-4}$ N/m, respectively.
The estimates are comparable, especially with very approximate values of parameters.

\begin{figure}
 \centering
 \includegraphics[keepaspectratio=true, width=80mm]{SurfactantGeneral}
 % SurfactantGeneral.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{Schematic depiction of the action of surfactants showing two chemical species, A and B, in both the dissolved and adsorbed states.}
 \label{fig:SurfactantGeneral}
\end{figure}

\section{Surfactants}
Surfactants are chemical compounds, which preferentially adsorb to the liquid interface. 
For example, consider a a common surfactant, sodium dodecyl sulfate (SDS). 
The molecular structure of this compound consists of a hydrophillic (i.e. water-loving) head and a hydrophyllic (i.e. water-hating) tail (see Figure~\ref{fig:SurfactantGeneral}).
When mixed with water, SDS dissolves in it.
But the molecules that can adsorb to the interface with their heads immersed in water and tails emerging out of water have a lower energy configuration than that of the molecules in the bulk, where the hydrophobic tails are surrounded by water molecules.
Each such molecule adsorbed on the water interface lowers the energy of the interface, and depending on the area density of the adsorbed molecules, reduces the excess energy of the interface per unit area.
Since excess energy per unit area is equivalent to surface tension, surfactants when mixed with water reduce the surface tension of water.

\begin{figure}
 \centering
 \includegraphics[keepaspectratio=true, width=100mm]{Equilibrium}
 % SurfactantGeneral.pdf: 0x0 px, 300dpi, 0.00x0.00 cm, bb=
 \caption{Measurements of the equilibrium behaviour of SDS. The adsorbed concentration of SDS ($\Gamma$) and the surface tension ($\sigma$) for different dissolved concentrations. }
 \label{fig:Equilibrium}
\end{figure}

\section{Conclusion}
In this chapter, we introduced the concept of excess surface energy and surface tension for liquid interfaces and showed that they are equal.
We also described the physical origin of the liquid state and that of surface tension.
