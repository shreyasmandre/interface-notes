FROM debian:jessie

MAINTAINER Shreyas Mandre <shreyas.mandre@warwick.ac.uk>

# RUN apt-get update && apt-get install -y texlive-full latexml make tzdata
RUN apt-get update && apt-get install -y texlive-latex-extra texlive-fonts-extra latexml make tzdata git
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
